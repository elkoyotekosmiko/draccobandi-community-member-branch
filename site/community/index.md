
# Comunidad

```{toctree}
:maxdepth: 1
:hidden:
:glob:
./*
```


## Miembros

Aunque GuayaHack es una iniciativa creada por {doc}`/community/member/space/jdsalaro/index`, es un esfuerzo colaborativo de tod@s para tod@s, éstos son sus miembros:

## Voluntarios

### Moderadores

Todos los moderadores son tutores.

1. {doc}`/community/member/space/jdsalaro/index`
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Tutores

Todos los tutores son participantes.

1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD

### Participantes

1. {doc}`/community/member/space/danteboe/index`
1. {doc}`/community/member/space/draccobandi/index` 
1. NOMBRE_DISCORD
1. NOMBRE_DISCORD
