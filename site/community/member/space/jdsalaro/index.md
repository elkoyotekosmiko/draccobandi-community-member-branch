
```{post} 2023-07-18
:author: "@jdsalaro"
:tags: fundador, moderador
:category: espacios
:language: Español
:location: Colombia
:excerpt: 1
```

# @jdsalaro's Space

Hola soy `@jdsalaro`! 

Me gustan los computadores, la música electrónica, los deportes y los idiomas. 

Mi artista favorito es [Avicii](https://en.wikipedia.org/wiki/Avicii).

Éste es mi espacio en GuayaHack.

## TODO

```{figure} index.md-data/tux.png
---
name: tux
---
Tux
```

```console
$ git status 
On branch master
Your branch is up to date with 'origin/master'.

nothing to commit, working tree clean
```
