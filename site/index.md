
# Main @ GuayaHack

Bienvenid@s a GuayaHack, un grupo de estudio de Programación que surgió como idea en [/r/Colombia](https://www.reddit.com/r/Colombia/comments/151fkiz/con_una_prima_y_un_amigo_armaremos_un_grupo_de).


## Community

```{toctree}
:maxdepth: 0
:hidden:
:caption: "# GuayaHack"
# Community <community/index.md>
# Posts <posts/index.md>
```
Puedes aprender más sobre GuayaHack y sus miembros en [Comunidad](community/index.md) o leyendo las [Reglas](community/rules.md) y nuestro [Manifiesto](community/manifest.md)

## Guías, Materiales y Otros

Esa tal documentación no existe, por eso hay que crearla. Escriba primero, piense despues :), por ejemplo en la [WIKI](https://guayahack.co/posts/category/wiki/)



## Indices and tables

* {ref}`genindex`
* {ref}`modindex`
* {ref}`search`
