
```{post} 2023-07-18
:author: "@jdsalaro"
:tags: newbie, backend, frontend, security, data, infrastructure
:category: tareas
:language: Español
:location: Alemania
:excerpt: 1
```

# Presentándose y Entendiendo GGG (GuayaHack, Git, GitLab)

Ésta es la primera tarea con la que todo newbie deberá comenzar. 

## Objetivo

Tú objetivo será `1` crear tu espacio en GuayaHack y tu `index.md` de presentación similar a {doc}`/community/member/space/jdsalaro/index` y `2` agregar tu nombre a la lista de miembros en {doc}`/community/index` enlazándolo con `1`. 


## Guía 

### Clonando main @ GuayaHack

```
$ git clone git@gitlab.com:guayahack/main.git
```

### Creando tu Branch

Lorem Ipsum

### Realizando Cambios y Haciendo Push

Lorem Ipsum


### Abriendo un Merge/Pull Request

Lorem Ipsum


### Verificando Cambios

Lorem Ipsum






## Materiales y Herramientas

Lorem Ipsum



## Problemas Comunes

### Problema

Lorem Ipsum

#### Solución

Lorem Ipsum


