```{post} 2023-06-30
:author: GuayaHack
:tags: mytag
:category: wiki
:language: Español
:excerpt: 1
```

# Áreas, Niveles y Progreso en GuayaHack

Corta introducción

## Áreas

BACK (back-end), DATA (data-analysis), FRONT (front-end), INFRA (infrastructure), SEC (security), MATH (scientific computing), SOFT (soft skills).

## Niveles

Novice, Experienced, Professional


```{figure} template.md-data/tux.png
---
name: guayahack-main
---
Tux
```

## Progreso

Charlas, Lectura, Tareas, Proyectos

### H3

## Contribuciones 

{doc}`@jdsalaro </community/member/space/jdsalaro/index>`,

